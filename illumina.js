// Illumina provides eve related services, and is primarily a discord bot, but
// it also has web server and ESI (eve swagger interface) integrations.
//'use strict'

// The Illumina object holds references to things that should be accessible
// outside the scope of the main javascript file.
global.Illumina = {}

// Imported Libraries
// *****************************************************************************
Illumina.Lib = {}
Illumina.Lib.EventEmitter    = require('events')
Illumina.Lib.Websocket       = require('ws')            //Websockets
Illumina.Lib.Discord         = require('discord.js')
Illumina.Lib.SQLite          = require('better-sqlite3')
Illumina.Lib.https           = require('https')
Illumina.Lib.FileSys         = require('fs')            //File System
Illumina.Lib.Express         = require('express')
Illumina.Lib.Express.Session = require('express-session')
Illumina.Lib.Passport        = require('passport')
Illumina.Lib.Passport.Eve    = require('passport-eveonline-sso')
// -----------------------------------------------------------------------------

// Log File
Illumina.Log = Illumina.Lib.FileSys.
createWriteStream(__dirname + '/Illumina.log', {flags : 'w'})

// Database setup
Illumina.Database = require('./database.js')

// Load Configuration
// *****************************************************************************
Illumina.Config  = require('./config.js')
Illumina.Secrets = require('./secrets.js')
// -----------------------------------------------------------------------------

// Setup Services
// *****************************************************************************

// Storage root for globally accessible service funtions or data.
Illumina.Service = {}

Illumina.Services = new Map()
Illumina.Services.set("discord", require('./services/discord.js'))
Illumina.Services.set("zkillboard", require('./services/zkillboard.js'))
Illumina.Services.set('webserver', require('./services/webserver.js'))

console.log('[Illumina] Starting services...')
Illumina.Services.forEach((command, name) => {command.Start()})
console.log('[Illumina] Service startup complete.')
// Setup Discord and assign message handler.
// -----------------------------------------
Illumina.Service.Discord.Client.on('message',onDiscordMessage)

//
// Discord message handler.
//
function onDiscordMessage (message){
    // Do not bother processing messages sent by bots.
    if(message.author.bot) return
    // Prevent the bot from responding if not mentioned
    //if(!receivedMessage.isMentioned(Illumina.Discord.Client.user)){return false}

    let prefix = Illumina.Config.prefix
    let command = message.content
    
    // True if our bot was directly mentioned
    let hasMention = message.isMentioned(Illumina.Service.Discord.Client.user)
    //TODO: Remove Mention From Command

    let hasPrefix = false
    // If the command is longer than the prefix and...
    if(command.length >= prefix.length)
    // ...the characters match, then it is a prefixed command.
    if(command.substr(0, prefix.length) == prefix) hasPrefix = true

    // Remove the prefix from the command if it exists.
    if (hasPrefix) command = command.substr(prefix.length)

    // Try to process the command
    if(hasPrefix||hasMention) processCommand(message, command)
}

//
// Processes a command for handoff to a command module.
// Called by the discord message handler event.
//
function processCommand(message, fullCommand) {
    // Split the message up in to pieces for each space
    let splitCommand = fullCommand.split(" ")
    // The first word directly after the prefix is the command
    let primaryCommand = splitCommand[0]
    if(primaryCommand) primaryCommand = primaryCommand.toLowerCase()
    // All other words are arguments/parameters/options for the command
    let arguments = splitCommand.slice(1)

    console.log("[Illumina] Command received: " + primaryCommand)
    console.log("Arguments: " + arguments) // There may not be any arguments

    // If the command exists, execute it.
    if(Illumina.Commands.has(primaryCommand)) {
        Illumina.Commands.get(primaryCommand).Execute(arguments, message)
    }
    // If the command does not exist, complain about it.
    else {
        console.log("[WARN] Unknown Command \"" + primaryCommand  + "\"")
    }

}

// References to Eve related objects
Illumina.Eve = Illumina.Eve || {}
    
// Setup Commands
// --------------
Illumina.Commands = new Map()
Illumina.Commands.set("help"  , require('./commands/help.js'  ))
Illumina.Commands.set("qiwi"  , require('./commands/qiwi.js'  ))
Illumina.Commands.set("notify", require('./commands/notify.js'))
Illumina.Commands.set("tz"    , require('./commands/tz.js'    ))
Illumina.Commands.set("zkill" , require('./commands/zkill.js' ))

console.log('[Illumina] Initializing Commands...')
Illumina.Commands.forEach((command, name) => {command.Init()})
console.log('[Illumina] Command initialization complete.')

// Setup Periodic Tasks
// --------------------
// The interval object for executing our periodic tasks.
Illumina.TaskTimer = null

Illumina.Tasks = new Map()
//Illumina.Tasks.set("TickLog", require('./tasks/ticklog.js'))

console.log("Initializing Tasks...")
Illumina.Tasks   .forEach((task   , name) => {task   .Init()})
console.log("Tasks and Commands Initialized.")

//Setup call to task execution function on an interval.
Illumina.TaskTimer = setInterval (doTasks, 60 * 1000);

//
// Executes tasks
//
function doTasks () {
    console.log("[Illumina:Tasks] Executing periodic tasks.")
    Illumina.Tasks.forEach(function(task, name){task.Execute()})
}