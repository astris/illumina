//
// Subscribes to zKillboard websocket and parses killmails before
// firing an event with the parsed killmail as its parameter.
//
'use strict'

// An alias variable to keep code line lengths short.
var zkb

// A saved value for whether we should reconnect a dropped connection
var doReconnect = false

// Properties used for reconnect intervals and progressive backoff.
const connectIntervalStart = 500	//doubled before it is first used!
const connectIntervalMax   = 60000
var connectInterval = connectIntervalStart

exports.Start = function(){

	doReconnect = true

	console.log('[Service:zKillboard] Starting Service...')
	Illumina.Service.zKillboard = Illumina.Service.zKillboard || {}
	zkb = Illumina.Service.zKillboard

	// Setup Event Emitter
	zkb.Events = new Illumina.Lib.EventEmitter()
	zkb.on = function() {zkb.Events.on.apply(zkb.Events,arguments)}

	// Set Connection Parameters and Connect to zKillboard
	zKillConnect()
}

exports.Stop = function(){
	doReconnect = false
	zkb.Socket.close(1000,'The client is shutting down.')
}

exports.Restart = function() {
	Stop()
	Start()
}

function zKillConnect() {
	console.log('[Service:zKillboard] Connecting to websocket...')
	zkb.Socket = new Illumina.Lib.Websocket('wss://zkillboard.com:2096')
	zkb.Socket.onopen    = zKillOnOpen
	zkb.Socket.onmessage = zKillOnMessage
	zkb.Socket.onclose   = zKillOnClose
	zkb.Socket.onerror   = zKillOnError
}

function zKillOnOpen() {
	connectInterval = connectIntervalStart
	console.log('[Service:zKillboard] Requesting subscription...')
	zkb.Socket.send(JSON.stringify({"action":"sub","channel":"killstream"}))
}

function zKillOnMessage(rawMessage) {
   	let killmail = JSON.parse(rawMessage.data);
   	console.log('[Service:zKillboard] Received Killmail ID:' + killmail.killmail_id)
   	zkb.Events.emit('receive',killmail)
}

function zKillOnClose() {
	console.log('[Service:zKillboard] Socket Closed')
	if(doReconnect){
		if (connectInterval <= connectIntervalMax) connectInterval = connectInterval * 2
		if (connectInterval >  connectIntervalMax) connectInterval = connectIntervalMax
		console.log('[Service:zKillboard] Reconnecting in ' + connectInterval + 'ms.')
		setTimeout(zKillConnect, connectInterval);
	} else {
		console.log('[Service:zKillboard] No Reconnect Scheduled')
	}
}

function zKillOnError(error) {
	console.error('[Service:zKillboard] Socket Error: ', error.message, 'Closing socket');
	zkb.Socket.close();
}