// Web server manager and supporting software.  Responds to page load requests.

// Information we need to make the eve sso work.
let Credentials = {}
Credentials.clientID     = Illumina.Secrets.Eve.ClientID
Credentials.clientSecret = Illumina.Secrets.Eve.Secret
Credentials.callbackURL  = Illumina.Secrets.Eve.Callback
Credentials.scope        = ''

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete EVE Online profile is
//   serialized and deserialized.
Illumina.Lib.Passport.serializeUser(function(user, done) {
  done(null, user);
});

Illumina.Lib.Passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

// *****************************************************************************
// Use the EveOnlineSsoStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and EVE Online Character
//   profile), and invoke a callback with a user object.
const EvePassportStrategy =
  new Illumina.Lib.Passport.Eve.Strategy(Credentials, StrategyVerify)

Illumina.Lib.Passport.use(EvePassportStrategy)


// -----------------------------------------------------------------------------

// 
App = Illumina.Lib.Express()

// Parameters to use to set up the https server.
Port = 37302
Cert = Illumina.Lib.FileSys.readFileSync('server.cert', 'utf8')
Key  = Illumina.Lib.FileSys.readFileSync('server.key' , 'utf8')
SSLCerts = {key: Key, cert: Cert}
Server = Illumina.Lib.https.createServer(SSLCerts, App)
Server.listen(Port, function () {
    console.log('Illumina listening on port '+ Port +'!')
    })

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }

  res.redirect('/login')
}

// Configure Express

// Create a new Express application.
App.set('port', process.env.PORT || 3000);

// Configure view engine to render EJS templates.
App.set('views', __dirname + '/webserver/views');
console.log('Views Directory: '+__dirname + '/webserver/views')
App.set('view engine', 'ejs');

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
// App.use(require('cookie-parser')());
// App.use(require('body-parser').urlencoded({ extended: true }));
App.use(Illumina.Lib.Express.Session({ secret: Illumina.Secrets.SessionSig , resave: true, saveUninitialized: true }));

// Initialize Passport and restore authentication state, if any, from the
// session.
App.use(Illumina.Lib.Passport.initialize());
App.use(Illumina.Lib.Passport.session());

App.get('/', function(req, res) {
  res.render('index', { user: req.user });
});

App.get('/profile', ensureAuthenticated, function(req, res) {
  res.render('profile', { user: req.user });
});

App.get('/login', function(req, res) {
  res.render('login', { user: req.user });
});

// GET /auth/eveonline
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in EVE Online authentication will involve
//   redirecting the user to EVE Online.  After authorization, EVE Online
//   will redirect the user back to this application at /auth/eveonline/callback
//   This path can be almost anything.
App.get('/auth/eveonline',
  Illumina.Lib.Passport.authenticate('eveonline-sso'),
  function(req, res){
    // The request will be redirected to EVE Online for authentication, so this
    // function will not be called.
  });

// GET /auth/eveonline/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
//   This path is the same as specified in the application on the
//   EVE Online Developers site. This is also the same as your callbackURL.
App.get('/eve-callback', 
  Illumina.Lib.Passport.authenticate('eveonline-sso', { failureRedirect: '/error' }),
  function(req, res) {
    res.redirect('/');
  });

App.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

exports.App  = App
exports.Port = Port
exports.Cert = Cert
exports.Key  = Key
exports.Credentials = Credentials
exports.Server = Server
exports.Start = function(){
  
}

// -----------------------------------------------------------------------------
// Associates a set of credentials (accessToken, refreshToken, and profile for
// Eve SSO) with a user and invokes the callback (done) in various ways.
// If a user is found:
// return done (null,userObject)
// If not found, user is set to false and an optional error string can be added:
// return done (null, false, {message: 'Bad Password'})
// If an error, the first argument is an error object:
// return done(error)
// If a user is not found, done is invoked with null and a boolean false
function StrategyVerify(accessToken, refreshToken, profile, done) {
  //Asynchronous verification for effect...
  process.nextTick(()=>{
      
    // To keep the example simple, the user's EVE Online Character profile is returned to
    // represent the logged-in user.  In a typical application, you would want
    // to associate the EVE Online Character with a user record in your database,
    // and return that user instead.

    // You could save the tokens to a database and/or call EVE Swaggger Interface (ESI) resources.
    // If you desire to use ESI, be sure to configure your application at https://developers.eveonline.com/applications)
    // and specify your scopes with EVEONLINE_SSO_SCOPES

    // The refreshToken will only be set if you are using scopes

    console.log('=== New Login ===');
    console.log('accessToken:', accessToken);
    console.log('refreshToken:', refreshToken);
    console.log('profile:', profile);

    return done(null, profile);
  })
}

function PassportSerialize(userToSerialize, done){
  
}