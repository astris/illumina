// Instantiate discord client
exports.Client = new Illumina.Lib.Discord.Client({autoReconnect:true})
Illumina.Service.Discord = {}
Illumina.Service.Discord.Client = exports.Client

// Assign event handlers
exports.Client.on('ready', onReady)
exports.Client.on('error', onError)

exports.Start = function(){
	// Login to discord
	DiscordLogin()
}

// ***************************************************************
// Function Definitions

//
// Function to login to discord.  Retries after 10s on failure
//
function DiscordLogin(){
    // Connect to discord and start doing what bots do.
    exports.Client.login(Illumina.Secrets.Discord.BotToken)
    .then(() => {
        console.log('[Service:Discord]: Client logged in.')
    })
    .catch((error)=>{
        console.error('[Service:Discord] Client Login Error: ', error)
        setTimeout(function() { DiscordLogin();}, 10000)
    })
}

//
// Discord 'ready' event handler
//
function onReady() {
    console.log("Connected as " + exports.Client.user.tag)
    // List servers the bot is connected to
    console.log("Servers:")
    exports.Client.guilds.forEach((guild) => {
        console.log(" - " + guild.name)
    })
}

//
// Discord 'error' event handler
//
function onError(error) {
	console.error('[Discord] Client Error: ', error);
}