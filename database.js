// ----------------------------------------------------------------------------\
// Main Database
exports.main = {}
exports.main.db = null
exports.main.filename = './database/illumina.db'
exports.main.description = 'Illumina main database'
exports.main.defineTables = []
exports.main.defineTables.push(
	'CREATE TABLE IF NOT EXISTS main (' +
	'type TEXT, ' +
	'name TEXT, ' +
	'key TEXT, ' +
	'value TEXT, ' +
	'UNIQUE(type,name,key)' +
	');'
)

exports.main.querryFormat =
	'SELECT value FROM main WHERE type = ? AND name = ? AND key = ?'
exports.main.insertFormat =
	'INSERT OR REPLACE INTO main (type, name, key, value) VALUES(?,?,?,?)'

exports.main.get = function(Type,Name,Key){
	let Main = exports.main
	if(Main.db)
		return Main.db.prepare(Main.querryFormat).get(Type,Name,Key).value
	else{
		let Return
		exports.open(Main)
		Return =
			Main.db.prepare(Main.querryFormat).get(Type,Name,Key).value
		exports.close(Main)
		return Return
	}

}

exports.main.set = function(...args){
	let dbInfo = exports.main
	if(dbInfo.db){
		return dbInfo.db.prepare(dbInfo.insertFormat)
				.run(...args)
	}else{
		let Return
		exports.open(dbInfo)
		Return = dbInfo.db.prepare(dbInfo.insertFormat)
				.run(...args)
		exports.close(dbInfo)
		return Return
	}	
}
// ----------------------------------------------------------------------------/


exports.open = function(dbInfo){
	if(dbInfo.db){
		console.error('[DB] '+ dbInfo.description +' is already open.')
	}
	dbInfo.db = Illumina.Lib.SQLite(dbInfo.filename)
	dbInfo.defineTables.forEach((table)=>{
		dbInfo.db.prepare(table).run()
	})
	console.log('[DB] ' +dbInfo.description+ ' opened')
}

exports.close = function(dbInfo){
		if(dbInfo.db){
		dbInfo.db.close()
		dbInfo.db = null
		console.log('[DB] '+dbInfo.description+' database closed')
	}else{
		console.error('[DB] '+dbInfo.description+' database is already closed')
	}
}
