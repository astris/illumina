// =============================================================================
// This class stores a time value with a resolution of 1 second that can
// represent any point in a week long interval.
// =============================================================================
'use strict'

class cWeekTime{

	constructor(Ticks,TZ){
		this._tz = 'utc'
		
		if(Ticks){
			this.time = Ticks
		}else{
			this._ticks = 0
		}

		if(TZ) this._tz = TZ.toString()
	}

	toString(){
		return this.string24st
	}

	valueOf(){
		return this.time
	}

	[Symbol.toPrimitive](hint) {
		if(hint == 'number') return this.valueOf()
		if(hint == 'string') return this.toString()
		if(hint == 'default') return this.toString()
		return undefined
	}

	get clone(){
		return new cWeekTime(this._ticks,this.tz)
	}

	// ------------------------------------------------------------------------\
	// Getter and Setter for the entire time value
	set time(Time){
		this._ticks = cWeekTime.enforceRange(
			Time - cWeekTime.tzTable.get(this.tz),
			cWeekTime.weekMultiplier)
	}

	get time(){
		return cWeekTime.enforceRange(
			this._ticks + cWeekTime.tzTable.get(this.tz),
			cWeekTime.weekMultiplier)
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Various string representations in 12 and 24 hour formats.  'm' types
	// show precision to the minute, 's' types show precision to the second.
	// 't' types show the set time zone.

	get string24m(){
		return this.stringDay + " "
		+ this.stringHr24 + ":"
		+ this.stringMin
	}

	get string12m(){
		return this.stringDay + " "
		+ this.stringHr12 + ":"
		+ this.stringMin + " "
		+ this.period
	}

	get string12mt(){
		return this.string12m + " " + this.tz.toUpperCase()
	}

	get string24s(){
		return this.string24m + ":" 
		+ this.stringSec
	}

	get string12s(){
		return this.stringDay + " "
		+ this.stringHr12 + ":"
		+ this.stringMin + ":"
		+ this.stringSec + " "
		+ this.period
	}

	get string24st(){
		return this.string24s + " " + this.tz.toUpperCase()
	}

	get string12st(){
		return this.string12s + " " + this.tz.toUpperCase()
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Getter, Setter, and String Converter for the Day
	get day(){
		// Constrain the value to within a week time period using modulus
		// division.  There are only 7 week days (0 - 6).
		let Day = this.time % cWeekTime.weekMultiplier

		// Return the count of whole days
		Day = parseInt(Day/cWeekTime.dayMultiplier)
		return Day
	}

	set day(DayIndex){
		// Constrain the index to be 6 or less and positive.  Weeks can't
		// roll over to another in either direction, anyway.
		DayIndex = DayIndex % 7
		if (DayIndex < 0) DayIndex += 7

		this.time += (DayIndex - this.day) * cWeekTime.dayMultiplier
	}

	get stringDay(){
		switch(this.day){
			case 0: return "Sunday"
			case 1: return "Monday"
			case 2: return "Tuesday"
			case 3: return "Wednesday"
			case 4: return "Thursday"
			case 5: return "Friday"
			case 6: return "Saturday"
			default: return "Invaid Day Index -> " + this.day
		}
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Getter and Setter for the hour
	set hr(Hour){
		this.time += (Hour - this.hr) * cWeekTime.hourMultiplier
	}

	get hr(){
		let DayTime = this.time % cWeekTime.dayMultiplier
		return parseInt(DayTime / cWeekTime.hourMultiplier)
	}

	get stringHr24(){
		let Return = ""
		Return += this.hr
		return Return
	}

	get stringHr12() {
		let Return = ""
		let Hour = this.hr
		if (Hour == 0) Hour = 12
		while (Hour > 12) Hour -=12
		Return += Hour 
		return Return
	}

	get period() {
		if(this.hr < 12) return "AM"
		else return "PM"
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Getter and Setter for the minute
	set min(Min){
		this.time += (Min - this.min) * cWeekTime.minuteMultiplier
	}

	get min(){
		let DayTime = this.time % cWeekTime.dayMultiplier
		DayTime = DayTime % cWeekTime.hourMultiplier
		return parseInt(DayTime / cWeekTime.minuteMultiplier)
	}

	get stringMin(){
		return cWeekTime.leading0(this.min)
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Getter and Setter for the second
	set sec(Sec){
		this.time += Sec - this.sec
	}

	get sec(){
		let DayTime = this._ticks % cWeekTime.dayMultiplier
		DayTime = DayTime % cWeekTime.hourMultiplier
		DayTime = DayTime % cWeekTime.minuteMultiplier
		return parseInt(DayTime)
	}

	get stringSec(){
		return cWeekTime.leading0(this.sec)
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Getter and setter for the time zone.  Setting the time zone converts
	// from the old time zone to the new one.
	set tz(NewTz){
		if (cWeekTime.tzTable.get(NewTz) != undefined) this._tz = NewTz
		else console.warn('[cWeekTime] WARN: Unknown Time Zone "' + NewTz + '"')
	}

	get tz(){
		return this._tz
	}
	// ------------------------------------------------------------------------/

	// ------------------------------------------------------------------------\
	// Returns a new cWeekTime converted into the target time zone.

	inTz(ToTz){
		let Return =this.clone
		Return.tz = ToTz
		return Return
	}
	// ------------------------------------------------------------------------/
}

cWeekTime.minuteMultiplier = 60
cWeekTime.hourMultiplier   = 60 * cWeekTime.minuteMultiplier
cWeekTime.dayMultiplier    = 24 * cWeekTime.hourMultiplier
cWeekTime.weekMultiplier   =  7 * cWeekTime.dayMultiplier

cWeekTime.tzTable = new Map()
cWeekTime.tzTable.set('aest',  10 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set('aedt',  11 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'utc',  0 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'est', -5 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'edt', -4 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'cst', -6 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'cdt', -5 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'mst', -7 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'mdt', -6 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'pst', -8 * cWeekTime.hourMultiplier)
cWeekTime.tzTable.set( 'pdt', -7 * cWeekTime.hourMultiplier)

cWeekTime.enforceRange = function (Value,Range){
	Value = Value % Range
	while (Value < 0) Value += Range
	return Value
}

// ------------------------------------------------------------------------\
// Returns a string with a leading 0 if number is less than 10
cWeekTime.leading0 = function (Number){
	let Return = ""
	if(Number < 10) Return += "0"
	Return += Number
	return Return
}
// ------------------------------------------------------------------------/

module.exports = cWeekTime