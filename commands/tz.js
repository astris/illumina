var cWeekTime = require("../classes/cWeekTime.js")

exports.ShortHelp = "Convert and display a time in several time zones."

var OriginTime = new cWeekTime()

var RegEx12Hour
var RegEx24Hour
var tzList

// Command Initialization
exports.Init = function(){
    console.log("[Tz] Initializing...")
    tzList = ['utc','cdt','edt','aedt']
	
	// Construct regular expressions to parse day and time input
	// ---------------------------------------------------------
	let RXPartWeekdays = "(sun|mon|tue|wed|thu|fri|sat)"
	let RXPartTime12Hr = "(1[0-2]|0?[1-9]):([0-5][0-9]) ?([AP]M)"
	let RXPartTime24Hr = "(2[0-3]|[01]?[0-9]):([0-5]?[0-9])"
	let RXPartTimeZone = "(" + Array.from(cWeekTime.tzTable.keys()).join("|") +")"
	
	RegEx12Hour = new RegExp("^ ?" + RXPartWeekdays +
								 " ?" + RXPartTime12Hr +
								 " ?" + RXPartTimeZone, "im")
	RegEx24Hour = new RegExp("^ ?" + RXPartWeekdays +
								 " ?" + RXPartTime24Hr +
								 " ?" + RXPartTimeZone, "im")
}

// Command Execution
exports.Execute = function(arguments, message) {
	console.log("[Command:Tz] Command received.")
    if(arguments.length == 0){
    	NoArguments(message)
    	return
    } else {
    	DoArgs(arguments, message)
    }
}

// Execution for the command with no arguments
function NoArguments(message){
	console.log("[Command:Tz] ERROR: No Arguments")
	message.author.send("[Error] The timestamp command requires a timestamp " +
		                 "to convert and display.")
}

// Execution for the command if arguments are present.
function DoArgs(arguments, message){
	if(ParseArgs(arguments)){
		console.log("[Command:Tz] Argument 0: " + arguments[0])
		const newEmbed = new Illumina.Lib.Discord.RichEmbed()
			.setTitle("Origin Time: " + OriginTime.string12mt)
			.setColor([55,55,55])
			//.setDescription(OriginTime.string12mt)
			.setTimestamp()//(arguments.join(" "))
		for(let i = 0; i<tzList.length; i++){
			newEmbed.addField(
				tzList[i].toUpperCase(),
				"> " + OriginTime.inTz(tzList[i]).string12m,
				true
			)
		}
		message.channel.send(newEmbed)
	}
}

function ParseArgs(arguments){
	let Return = false
	// Join the argument array into a single line for regex matching.
	let JoinedArgs = arguments.join(" ")
	// Variable to hold the array of results of a regex match.
	let RegExParse
	
	// Try to match and process 24 hour format.
	if (RegExParse = RegEx24Hour.exec(JoinedArgs)){
		console.log("[Command:Tz] 24 Hour Format Time Detected.")
		console.log("[Command:Tz] Day      : " + RegExParse[1])
		console.log("[Command:Tz] Hour     : " + RegExParse[2])
		console.log("[Command:Tz] Minute   : " + RegExParse[3])
		console.log("[Command:Tz] Time Zone: " + RegExParse[4])
		
		OriginTime.tz  = RegExParse[4]
		OriginTime.day = DayToNumber(RegExParse[1])
		OriginTime.hr  = parseInt(RegExParse[2])
		OriginTime.min = parseInt(RegExParse[3])
		
		Return = true
	}
	
	// Try to match and process 12 hour format.
	else if (RegExParse = RegEx12Hour.exec(JoinedArgs)){
		console.log("[Command:Tz] 12 Hour Format Time Detected.")
		console.log("[Command:Tz] Day      : " + RegExParse[1])
		console.log("[Command:Tz] Hour     : " + RegExParse[2])
		console.log("[Command:Tz] Minute   : " + RegExParse[3])
		console.log("[Command:Tz] AM/PM    : " + RegExParse[4])
		console.log("[Command:Tz] Time Zone: " + RegExParse[5])
		
		OriginTime.tz  = RegExParse[5].toLowerCase()
		OriginTime.day = DayToNumber(RegExParse[1])
		OriginTime.hr  = parseInt(RegExParse[2], 10)
		OriginTime.min = parseInt(RegExParse[3], 10)
		if(RegExParse[4].toLowerCase() == "am"){
			if(OriginTime.hr == 12) OriginTime.hr = 0;
		}
		
		if(RegExParse[4].toLowerCase() == "pm"){
			if(OriginTime.hr < 12) OriginTime.hr += 12;
		}
		
		
		Return = true
	}
	
	console.log("[Command:Tz] StartDay     : " + OriginTime.Day)
	console.log("[Command:Tz] StartHour    : " + OriginTime.Hour)
	console.log("[Command:Tz] StartMinute  : " + OriginTime.Minute)
	console.log("[Command:Tz] StartTimeZone: " + OriginTime.TimeZone)
	
	return Return
}

function DayToNumber(stringDay){
	stringDay = stringDay.toLowerCase()
	if (stringDay == 'sun') return 0
	if (stringDay == 'mon') return 1
	if (stringDay == 'tue') return 2
	if (stringDay == 'wed') return 3
	if (stringDay == 'thu') return 4
	if (stringDay == 'fri') return 5
	if (stringDay == 'sat') return 6
}

// Output when the help command gets help on this command.
exports.Help = function(arguments, message){
    message.channel.send("Displays a UTC time code in an embed that discord" +
                        " will convert into local time.")
}
