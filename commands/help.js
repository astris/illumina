exports.ShortHelp = "Lists or gets help on bot commands."

// Command Initialization
exports.Init = function(){
    console.log("[Help] Initializing...")
}

// Command Execution
exports.Execute = function(arguments, message) {
    if(arguments.length == 0){
    	NoArguments(message)
    	return
    } else {
    	DoArgs(arguments, message)
    }
}

// Execution for the command with no arguments
function NoArguments(message){
	let response = "[Help] Bot Command List:"
	for (const commandString of Illumina.Commands.keys()) {
        response += "\r\n" + commandString
        if(Illumina.Commands.get(commandString).ShortHelp)
        response += " - " +
        Illumina.Commands.get(commandString).ShortHelp
    }
    message.channel.send(response)
}

// Execution for the command if arguments are present.
function DoArgs(arguments, message){
	console.log("Getting help on \"" + arguments[0]+"\"")
    Illumina.Commands.get(arguments[0]).Help(arguments.slice(1), message)
}

// Output when the help command gets help on this command.
exports.Help = function(arguments, message){
    message.channel.send("Returns a summary of all known commands with no arguments " +
                         "or help on a specific command that is given as an argument.")
    console.log("[Help:Help] Arguments: " + arguments)
}
