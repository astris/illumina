exports.ShortHelp = "Subscribes to and displays killmails."
// Output when the help command gets help on this command.
exports.Help = function(arguments, message){
    message.channel.send("Displays a UTC time code in an embed that discord" +
                        " will convert into local time.")
}

//
// Initialize the zKill command
//
exports.Init = function(){
	// Report that we are initializing the zKill command
	console.log('[zKill] Initializing...')

	// Subscribe to killmail feed.
	Illumina.Service.zKillboard.on('receive', onKillmail)
}

//
// React to a zKill command issued in Discord
//
exports.Execute = function(arguments, message) {}

//
// This function executes when a parsed kill mail is received.
//
onKillmail = function(km){
	// Report receipt of killmail.
	console.log('[zKill] Receipt of killmail ID: ' + km.killmail_id)
	//Illumina.Discord.Client.channels.get('627770122223878164').send(km.zkb.url)
	//console.log(JSON.stringify(km, null, 4))
}