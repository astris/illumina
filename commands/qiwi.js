exports.ShortHelp = "Says \"Qiwi!\""

//Command Initialization
exports.Init = function(){
    console.log("[Qiwi] Initializing...")
}

var invokeCount = 0
var lastInvoker = ""

// Command Execution
exports.Execute = function(arguments, message) {
    if(arguments.length == 0){
    	NoArguments(message)
    	return
    } else {
    	DoArgs(arguments, message)
    }
}

// Execution for the command with no arguments
function NoArguments(message){
    if(message.member.nickname == lastInvoker){
        invokeCount++
        if (invokeCount<=1) {
            message.channel.send("Qiwi!")
        }
        else if (invokeCount<=2) {
            message.channel.send("I'm on to you, " 
                                + message.member.nickname + ".")
        }
    }else {
        invokeCount = 1
        lastInvoker = message.member.nickname
        message.channel.send("Qiwi!")
        console.log("Sender Guild Nickname:" + message.member.nickname)
    }	
}

// Execution for the command if arguments are present.
function DoArgs(arguments, message){
	console.log("Help Argument 0: " + arguments[0])
	if(arguments[0].toLowerCase() == "reset"){
		message.channel.send("Resetting Count...")
		invokeCount = 0
	}
}

// Output when the help command gets help on this command.
exports.Help = function(arguments, message){
    message.channel.send("Replies \"Qiwi!\" in the channel where the command" +
                        " is issued.")
}
