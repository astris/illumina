exports.ShortHelp = "Sends a message to the notification channel."

// Command Initialization
exports.Init = function(){
    console.log("[Notify] Initializing...")
}

// Command Execution
exports.Execute = function(arguments, message) {
    if(arguments.length == 0){
    	NoArguments(message)
    	return
    } else {
    	DoArgs(arguments, message)
    }
}

// Execution for the command with no arguments
function NoArguments(message){
	message.channel.send("[Error] The notify command requires a message to send " +
		                 "to the notification channel.")
}

// Execution for the command if arguments are present.
function DoArgs(arguments, message){
	console.log("[Command:Notify] Argument 0: " + arguments[0])
		var NotifyChannel = message.client.channels.find(channel => channel.name === "notifications")
		if(NotifyChannel){
			let MentionRole = message.guild.roles.find(role => role.name === "Notify")
			const newEmbed = new Illumina.Discord.RichEmbed()
			.setTitle("Notification From " + message.member.nickname + ".")
			.setColor([255,0,0])
			.setDescription(arguments.join(" "))
			NotifyChannel.send(MentionRole.toString(), newEmbed)
		}
		else {
			console.log("[Command:Notify] ERROR: Notification channel not found.")
		}
}

// Output when the help command gets help on this command.
exports.Help = function(arguments, message){
    message.channel.send("Sends whatever follows the command to the notification" +
                        " channel.")
}
